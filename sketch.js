let snake;
let res = 10;
let nutrition;
let w;
let h;

function setup() {
  createCanvas(400, 400);
  w = floor(width/res);
  h = floor(height/res);
  frameRate(10);
  snake = new Snake();  
  nutritionCoordinates();
  // nutrition = createVector
}

function nutritionCoordinates() {
  let x = floor(random(w));
  let y = floor(random(h));
  nutrition = createVector(x,y);
}

function keyPressed() {
  if (keyCode === LEFT_ARROW){
  snake.setDir(-1,0);
  }else if (keyCode === RIGHT_ARROW){
  snake.setDir(1,0);
  }else if (keyCode === DOWN_ARROW){
  snake.setDir(0,1);
  }else if (keyCode === UP_ARROW){
  snake.setDir(0,-1);
  }else if (key = 'A'){
  snake.grow();
  }
}

function draw() {
  // noStroke();
  scale(res);
  background(220);
  if (snake.eat(nutrition)) {
    nutritionCoordinates();
  }
  snake.update();
  snake.show();
  if (snake.endGame()){
    print("end");
    background(0,0,0);
    noLoop();
  }
  
  
  noStroke();
  fill(255,0,0);
  rect(nutrition.x, nutrition.y, 1, 1);
  
}